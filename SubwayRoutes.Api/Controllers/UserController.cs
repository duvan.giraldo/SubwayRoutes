﻿using Microsoft.AspNetCore.Mvc;
using SubwayRoutes.Api.Business.Models;
using SubwayRoutes.Api.Mappers;
using SubwayRoutes.Api.Services.Contracts.Services;
using SubwayRoutes.Api.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Get all Users
        /// </summary>
        /// <returns>Users</returns>
        [HttpGet]
        [Produces("application/json", Type = typeof(List<UserModel>))]
        public async Task<IActionResult> GetAll()
        {
            var user = await _userService.GetAllUsers();

            return Ok(user);
        }

        /// <summary>
        /// Add user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        [ProducesResponseType(401)]
        [Produces("application/json", Type = typeof(UserModel))]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]UserModel userModel)
        {
            var name = await _userService.AddUser(UserMapper.Map(userModel));

            return Ok(name);
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(UserModel))]
        public async Task<IActionResult> Get(int id)
        {
            var user = await _userService.GetUser(id);

            return Ok(user);
        }

        /// <summary>
        /// Delete user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Produces("application/json", Type = typeof(bool))]
        public async Task<IActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);

            return Ok();
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [Produces("application/json", Type = typeof(UserModel))]
        public async Task<IActionResult> Update([FromBody]UserModel user)
        {
            var name = await _userService.UpdateUser(UserMapper.Map(user));

            return Ok(name);
        }
    }
}
