﻿using SubwayRoutes.Api.Business.Models;
using SubwayRoutes.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.Mappers
{
    public static class UserMapper
    {
        public static User Map(UserModel model)
        {

            return new User()
            {
                Id = model.Id,
                Name = model.Name
            };

        }
    }
}
