﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubwayRoutes.Api.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ISubwayRoutesDBContext _subwayRoutesDBContext;

        public UserRepository(ISubwayRoutesDBContext subwayRoutesDBContext)
        {
            _subwayRoutesDBContext = subwayRoutesDBContext;
        }

        public async Task<UserEntity> Update(int idEntity, UserEntity updateEnt)
        {

            var entity = await Get(idEntity);

            entity.Name = updateEnt.Name;

            _subwayRoutesDBContext.Users.Update(entity);

            await _subwayRoutesDBContext.SaveChangesAsync();

            return entity;
        }

        public async Task<UserEntity> Add(UserEntity entity)
        {

            await _subwayRoutesDBContext.Users.AddAsync(entity);

            await _subwayRoutesDBContext.SaveChangesAsync();

            return entity;
        }

        public async Task<UserEntity> Get(int idEntity)
        {

            var result = await _subwayRoutesDBContext.Users
                .FirstOrDefaultAsync(x => x.Id == idEntity);

            return result;

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            return _subwayRoutesDBContext.Users.Select(x => x);
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _subwayRoutesDBContext.Users.SingleAsync(x => x.Id == id);

            _subwayRoutesDBContext.Users.Remove(entity);

            await _subwayRoutesDBContext.SaveChangesAsync();

        }

        public async Task<UserEntity> Update(UserEntity entity)
        {

            var updateEntity = _subwayRoutesDBContext.Users.Update(entity);

            await _subwayRoutesDBContext.SaveChangesAsync();

            return updateEntity.Entity;
        }
    }
}
