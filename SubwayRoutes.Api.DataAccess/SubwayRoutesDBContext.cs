﻿using Microsoft.EntityFrameworkCore;
using SubwayRoutes.Api.DataAccess.Contracts;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using SubwayRoutes.Api.DataAccess.EntityConfig;

namespace SubwayRoutes.Api.DataAccess
{

    public class SubwayRoutesDBContext : DbContext, ISubwayRoutesDBContext
    {
        public DbSet<UserEntity> Users { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseNpgsql("Host=localhost;Database=SubwayRoutesDB;Username=postgres;Password=root");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserEntityConfig.SetEntityBuilder(modelBuilder.Entity<UserEntity>());

            base.OnModelCreating(modelBuilder);

        }

        public SubwayRoutesDBContext(DbContextOptions options) : base(options) { }

        public SubwayRoutesDBContext()
        {

        }

    }
}
