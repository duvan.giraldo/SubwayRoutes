﻿using SubwayRoutes.Api.Business.Models;
using SubwayRoutes.Api.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayRoutes.Api.DataAccess.Mappers
{
    public static class UserMapper
    {
        public static UserEntity Map(User dto)
        {
            return new UserEntity()
            {
                Id = dto.Id,
                Name = dto.Name,
                Password = dto.Password
            };
        }

        public static User Map(UserEntity entity)
        {
            return new User()
            {
                Id = entity.Id,
                Name = entity.Name,
                Password = entity.Password
            };
        }
    }
}
